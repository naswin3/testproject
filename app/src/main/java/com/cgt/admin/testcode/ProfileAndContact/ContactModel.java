package com.cgt.admin.testcode.ProfileAndContact;

/**
 * Created by Admin on 02-01-2018.
 */

public class ContactModel {

    /**
     * id : 1
     * name : uudsds
     * mobile : 9037571082
     * profile_pic : http://museon.net/testapp/uploads/9037571082/mpro_72def7f.jpg
     * register_time : 2017-06-21 04:51:45am
     * expiration :
     * status : null
     */

    private String id;
    private String name;
    private String mobile;
    private String profile_pic;
    private String register_time;
    private String expiration;
    private Object status;

    public ContactModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getRegister_time() {
        return register_time;
    }

    public void setRegister_time(String register_time) {
        this.register_time = register_time;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }
}
