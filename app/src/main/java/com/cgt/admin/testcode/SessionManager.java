package com.cgt.admin.testcode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;


public class SessionManager {
	// Shared Preferences
	private SharedPreferences pref;
	
	// Editor for Shared preferences
	private Editor editor,editor2,editor3,editor4,editor5;
	
	// Context
	private Context _context;

	// Shared pref mode
	private int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "TestCode";
	
	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	private static final String IS_SELECTED_LIST = "IsSelectedIn";
	


	public static final String KEY_CUSTOMER_ID = "customerid";
	public static final String KEY_MOBILE = "mobilenumber";
	public static final String KEY_NAME = "name";
	public static final String KEY_IMAGE = "image";




	// Constructor

	@SuppressLint("CommitPrefEdits")
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	/**
	 * Create login session
	 * */
	public void createLoginSession(String customer_id, String mob,String name,String image){
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);
		// Storing name in pref
		editor.putString(KEY_CUSTOMER_ID,customer_id);
		editor.putString(KEY_MOBILE,mob);
		editor.putString(KEY_NAME,name);
		editor.putString(KEY_IMAGE,image);

		// commit changes
		editor.commit();
	}





	/**
	 * Check login method wil check user login status
	 * If false it will redirect user to login page
	 * Else won't do anything
	 * */
	public void checkLogin(){
		// Check login status
		if(!this.isLoggedIn()){
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(_context, LoginActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			_context.startActivity(i);
		}
		
	}




	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<>();
		// user name
		//user.put(KEY_CUSTOMER, pref.getString(KEY_CUSTOMER, null));

		user.put(KEY_CUSTOMER_ID, pref.getString(KEY_CUSTOMER_ID, null));
		user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, null));
		user.put(KEY_NAME, pref.getString(KEY_NAME, null));
		user.put(KEY_IMAGE, pref.getString(KEY_IMAGE, null));

		// return user
		return user;
	}

	/**
	 * Clear session details
	 * */
	public void logoutUser(){
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();

		
		// After logout redirect user to Loing Activity
		Intent i = new Intent(_context, LoginActivity.class);
		// Closing all the Activities
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		// Staring Login Activity
		_context.startActivity(i);
	}
	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn(){
		return pref.getBoolean(IS_LOGIN, false);
	}

	public boolean isSelectedIn(){
		return pref.getBoolean(IS_SELECTED_LIST, false);
	}
}
