package com.cgt.admin.testcode.ProfileAndContact;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgt.admin.testcode.R;
import com.cgt.admin.testcode.SessionManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.HashMap;


public class Profile extends Fragment{

    public Profile() {
        // Required empty public constructor
    }

    View v;
    SessionManager session;
    HashMap<String,String> user;
    TextView name;
    TextView mobile;
    ImageView image;
    String imagefilepath;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getContext());
        user = session.getUserDetails();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate(R.layout.fragment_profile, container, false);

        name=v.findViewById(R.id.name);
        mobile=v.findViewById(R.id.mobile);
        image=v.findViewById(R.id.UserImage);

        name.setText(user.get(SessionManager.KEY_NAME));
        mobile.setText(user.get(SessionManager.KEY_MOBILE));

        imagefilepath="file://"+user.get(SessionManager.KEY_IMAGE);
        ImageLoader.getInstance().displayImage(imagefilepath,image);



        return v;

    }


}
