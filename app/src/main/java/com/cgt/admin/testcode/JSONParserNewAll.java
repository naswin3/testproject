package com.cgt.admin.testcode;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Admin on 31-10-2016.
 */

public class JSONParserNewAll {

    StringBuilder result,result2;
    StringBuilder sbParams;
    URL urlObj;
    JSONObject jObj2 = null;JSONObject jObj3 = null;

    JSONArray jObj = null;
    HttpURLConnection conn;
    String charset = "UTF-8";
    String resultcode;


    public JSONObject SendHttpPosts(String URL, String method, JSONObject jsonObjSend,HashMap<String,String> params) {

        sbParams = new StringBuilder();
        int i = 0;

        if(params!=null)
        for (String key : params.keySet()) {
            try {
                if (i != 0){
                    sbParams.append("&");
                }
                sbParams.append(key).append("=")
                        .append(URLEncoder.encode(params.get(key), charset));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;

        }



        if(method.equals("POST")) {

            if (sbParams.length() != 0) {
                URL += "?" + sbParams.toString();
            }

            try {

                urlObj = new URL(URL);
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setReadTimeout(30000 /* milliseconds */);
                conn.setConnectTimeout(30000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept-Charset", charset);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(jsonObjSend));

                writer.flush();
                writer.close();
                os.close();


            } catch (Exception e) {
                e.printStackTrace();

            }
        }


        else if(method.equals("GET")){

            if (sbParams.length() != 0) {
                URL += "?" + sbParams.toString();
            }

            try {
                urlObj = new URL(URL);
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setReadTimeout(30000 /* milliseconds */);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept-Charset", charset);
                conn.setDoInput(true);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(method.equals("DELETE")){

            if (sbParams.length() != 0) {
                URL += "?" + sbParams.toString();
            }

            try {
                urlObj = new URL(URL);
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setReadTimeout(30000 /* milliseconds */);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("DELETE");
                conn.setRequestProperty("Accept-Charset", charset);
                conn.setDoOutput(true);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {

            int responseCode = conn.getResponseCode();

            BufferedReader in = null;

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                in = new BufferedReader(new
                        InputStreamReader(
                        conn.getInputStream()));

                result = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {

                    result.append(line);
                    break;
                }


            }
            else{


                try {
                    jObj3=new JSONObject("{\"Status\":\"failed\",\"Message\":\"Error Code "+responseCode+"\"}");
                    resultcode= String.valueOf(jObj3);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                InputStream ins = new ByteArrayInputStream(resultcode.getBytes("UTF-8"));

                in = new BufferedReader(new
                        InputStreamReader(
                        ins));

                result = new StringBuilder();
                String line2;

                while ((line2 = in.readLine()) != null) {

                    result.append(line2);
                    break;
                }

            }
        } catch (IOException e) {

            e.printStackTrace();

        }

        try {
            jObj2 = new JSONObject(result.toString());

        } catch (JSONException e) {

            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON Object
        return jObj2;

    }

    public JSONArray SendHttpPostA(String URL, String method, JSONObject jsonObjSend,HashMap<String,String> params) {

        sbParams = new StringBuilder();
        int i = 0;

        if(params!=null)
            for (String key : params.keySet()) {
                try {
                    if (i != 0){
                        sbParams.append("&");
                    }
                    sbParams.append(key).append("=")
                            .append(URLEncoder.encode(params.get(key), charset));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i++;

            }



        if(method.equals("POST")) {

            if (sbParams.length() != 0) {
                URL += "?" + sbParams.toString();
            }

            try {

                urlObj = new URL(URL);
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept-Charset", charset);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(jsonObjSend));

                writer.flush();
                writer.close();
                os.close();


            } catch (Exception e) {
                e.printStackTrace();

            }
        }


        else if(method.equals("GET")){

            if (sbParams.length() != 0) {
                URL += "?" + sbParams.toString();
            }

            try {
                urlObj = new URL(URL);
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept-Charset", charset);
                conn.setDoInput(true);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(method.equals("DELETE")){

            if (sbParams.length() != 0) {
                URL += "?" + sbParams.toString();
            }

            try {
                urlObj = new URL(URL);
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("DELETE");
                conn.setRequestProperty("Accept-Charset", charset);
                conn.setDoOutput(true);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {

            int responseCode = conn.getResponseCode();

            BufferedReader in = null;

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                in = new BufferedReader(new
                        InputStreamReader(
                        conn.getInputStream()));

                result = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {

                    result.append(line);
                    break;
                }


            }

        } catch (IOException e) {

            e.printStackTrace();

        }

        try {
            jObj = new JSONArray(result.toString());

        } catch (JSONException e) {

            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON Object
        return jObj;

    }










    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    public  JSONObject FileUpload(String URL,String FileUri,HashMap<String,String> params) {

        int serverResponseCode=0;
        String data = null;
        String fileName = FileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        String filesNames=fileName.substring(fileName.lastIndexOf("/")+1);
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 2 * 1024 * 1024;
        File sourceFile = new File(FileUri);
        sbParams = new StringBuilder();
        int i = 0;

        if(params!=null)
            for (String key : params.keySet()) {
                try {
                    if (i != 0){
                        sbParams.append("&");
                    }
                    sbParams.append(key).append("=")
                            .append(URLEncoder.encode(params.get(key), charset));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i++;

            }
        try {
            if (sbParams.length() != 0) {
                URL += "?" + sbParams.toString();
            }
            // open a URL connection to the Servlet
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(URL);

            // Open a HTTP  connection to  the URL
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"userfile\";filename=\""
                    + filesNames + "\"" + lineEnd);

            dos.writeBytes(lineEnd);

            // create a buffer of  maximum size
            bytesAvailable = fileInputStream.available();

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {

                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();
            final String response;
            Log.i("uploadFile", "HTTP Response is : "
                    + serverResponseMessage + ": " + serverResponseCode + serverResponseMessage);

            if (serverResponseCode == 200) {

                final HttpURLConnection finalConn = conn;


                BufferedReader in = null;
                try {
                    in = new BufferedReader(new
                            InputStreamReader(
                            finalConn.getInputStream()));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = in.readLine()) != null) {

                        result.append(line);

                        jObj2 = new JSONObject(result.toString());
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }




            }

            //close the streams //
            fileInputStream.close();
            dos.flush();
            dos.close();

        } catch (MalformedURLException ex) {


            ex.printStackTrace();


            Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
        } catch (Exception e) {


            e.printStackTrace();


            Log.e("Upload file", "Exception : "
                    + e.getMessage(), e);
        }
        return jObj2;



    }
}

