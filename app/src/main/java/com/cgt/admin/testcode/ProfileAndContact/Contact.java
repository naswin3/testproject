package com.cgt.admin.testcode.ProfileAndContact;



        import android.Manifest;
        import android.content.pm.PackageManager;
        import android.database.Cursor;
        import android.database.MatrixCursor;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.net.Uri;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.provider.ContactsContract;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.app.Fragment;
        import android.support.v4.content.ContextCompat;
        import android.telephony.PhoneNumberUtils;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.ProgressBar;

        import com.cgt.admin.testcode.MyCursorAdapter;
        import com.cgt.admin.testcode.R;

        import java.io.File;
        import java.io.FileOutputStream;

        import static com.cgt.admin.testcode.Global.contact;


public class Contact extends Fragment{
    MyCursorAdapter mAdapter;
    MatrixCursor mMatrixCursor;
    View v;
    private ProgressBar mRegistrationProgressBar;
    private static final int PICK_IMAGE_REQUEST = 2;
    int CurrentRequest=0;
    private static final int PICK_FILE_REQUEST = 1;
    LinearLayout  no_contact;
    public Contact() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


         v=inflater.inflate(R.layout.fragment_contact, container, false);

         no_contact=v.findViewById(R.id.no_contact);



        // The contacts from the contacts content provider is stored in this cursor
        mMatrixCursor = new MatrixCursor(new String[] { "_id","name","photo","details","Mobile"} );
        // Adapter to set data in the listview
        mAdapter = new MyCursorAdapter(getActivity(), mMatrixCursor);
        ListView lstContacts = (ListView) v.findViewById(R.id.lst_contacts);
        // Setting the adapter to listview

        lstContacts.setAdapter(mAdapter);


        // Creating an AsyncTask object to retrieve and load listview with contacts
        ListViewContactsLoader listViewContactsLoader = new ListViewContactsLoader();

       /// new contact_fetch().execute();
        if(isReadStorageAllowed()){


                listViewContactsLoader.execute();

        }
        else
        {
            requestStoragePermission();
        }



        return v;
    }


    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private void requestStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.READ_CONTACTS)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        if (CurrentRequest == PICK_FILE_REQUEST){

            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_CONTACTS},PICK_FILE_REQUEST);


        }
        else{

            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_CONTACTS},PICK_FILE_REQUEST);

        }

    }

    /** An AsyncTask class to retrieve and load listview with contacts */
    private class ListViewContactsLoader extends AsyncTask<Void, Void, Cursor>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mRegistrationProgressBar = (ProgressBar) v.findViewById(R.id.registrationProgressBar);
            mRegistrationProgressBar.setVisibility(ProgressBar.VISIBLE);



        }

        @Override
        protected Cursor doInBackground(Void... params) {
            Uri contactsUri = ContactsContract.Contacts.CONTENT_URI;

            // Querying the table ContactsContract.Contacts to retrieve all the contacts
            Cursor contactsCursor = getActivity().getContentResolver().query(contactsUri, null, null, null,
                    ContactsContract.Contacts.DISPLAY_NAME + " ASC ");

            if(contactsCursor.moveToFirst()){
                do{
                    long contactId = contactsCursor.getLong(contactsCursor.getColumnIndex("_ID"));


                    Uri dataUri = ContactsContract.Data.CONTENT_URI;

                    // Querying the table ContactsContract.Data to retrieve individual items like
                    // home phone, mobile phone, work email etc corresponding to each contact
                    Cursor dataCursor = getActivity().getContentResolver().query(dataUri, null,
                            ContactsContract.Data.CONTACT_ID + "=" + contactId,
                            null, null);


                    String displayName="";
                    String nickName="";
                    String homePhone="";
                    String mobilePhone="";
                    String workPhone="";
                    String photoPath="" + R.drawable.ic_avatar;
                    byte[] photoByte=null;
                    String homeEmail="";
                    String workEmail="";
                    String companyName="";
                    String title="";



                    if(dataCursor.moveToFirst()){
                        // Getting Display Name
                        displayName = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME ));
                        do{

                            // Getting NickName
                            if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE))
                                nickName = dataCursor.getString(dataCursor.getColumnIndex("data1"));

                            // Getting Phone numbers
                            if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)){
                                switch(dataCursor.getInt(dataCursor.getColumnIndex("data2"))){
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME :
                                        homePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE :
                                        mobilePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK :
                                        workPhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                }
                            }

                            // Getting EMails
                            if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE ) ) {
                                switch(dataCursor.getInt(dataCursor.getColumnIndex("data2"))){
                                    case ContactsContract.CommonDataKinds.Email.TYPE_HOME :
                                        homeEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                    case ContactsContract.CommonDataKinds.Email.TYPE_WORK :
                                        workEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                }
                            }

                            // Getting Organization details
                            if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)){
                                companyName = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                title = dataCursor.getString(dataCursor.getColumnIndex("data4"));
                            }

                            // Getting Photo
                            if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)){
                                photoByte = dataCursor.getBlob(dataCursor.getColumnIndex("data15"));

                                if(photoByte != null) {
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);

                                    // Getting Caching directory
                                    File cacheDirectory = getActivity().getBaseContext().getCacheDir();

                                    // Temporary file to store the contact image
                                    File tmpFile = new File(cacheDirectory.getPath() + "/wpta_"+contactId+".png");

                                    // The FileOutputStream to the temporary file
                                    try {
                                        FileOutputStream fOutStream = new FileOutputStream(tmpFile);

                                        // Writing the bitmap to the temporary file as png file
                                        bitmap.compress(Bitmap.CompressFormat.PNG,100, fOutStream);

                                        // Flush the FileOutputStream
                                        fOutStream.flush();

                                        //Close the FileOutputStream
                                        fOutStream.close();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    photoPath = tmpFile.getPath();
                                }

                            }

                        }while(dataCursor.moveToNext());


                        String details="";

                        // Adding id, display name, path to photo and other details to cursor

                        boolean click = false;

                        for(ContactModel contacts:contact)
                        {

                            String conts=contacts.getMobile();

                            if (PhoneNumberUtils.compare(conts, mobilePhone)) {
                                click=true;



                                break;
                            }




                        }
                       if(click) {


                           // Concatenating various information to single string



                           if (homePhone != null && !homePhone.equals(""))
                               details = "HomePhone : " + homePhone + "\n";
                           if (mobilePhone != null && !mobilePhone.equals(""))
                               details += "MobilePhone : " + mobilePhone + "\n";
                           if (workPhone != null && !workPhone.equals(""))
                               details += "WorkPhone : " + workPhone + "\n";
                           if (nickName != null && !nickName.equals(""))
                               details += "NickName : " + nickName + "\n";
                           if (homeEmail != null && !homeEmail.equals(""))
                               details += "HomeEmail : " + homeEmail + "\n";
                           if (workEmail != null && !workEmail.equals(""))
                               details += "WorkEmail : " + workEmail + "\n";
                           if (companyName != null && !companyName.equals(""))
                               details += "CompanyName : " + companyName + "\n";
                           if (title != null && !title.equals(""))
                               details += "Title : " + title + "\n";


                           mMatrixCursor.addRow(new Object[]{Long.toString(contactId), displayName, photoPath, details, mobilePhone});
                           click = false;

                       }



                    }

                }while(contactsCursor.moveToNext());
            }
            return mMatrixCursor;
        }

        @Override
        protected void onPostExecute(Cursor result) {
            // Setting the cursor containing contacts to listview
            mAdapter.swapCursor(result);
            mAdapter.notifyDataSetChanged();
            mRegistrationProgressBar.setVisibility(ProgressBar.GONE);


            int c=result.getCount();
            if(c==0)
            {


                    no_contact.setVisibility(View.VISIBLE);

                }
                else
                {

                    no_contact.setVisibility(View.GONE);
                }


        }
    }


}
