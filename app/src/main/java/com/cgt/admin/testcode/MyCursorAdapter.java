package com.cgt.admin.testcode;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cgt.admin.testcode.ProfileAndContact.ClickedContact;


public class MyCursorAdapter extends CursorAdapter {
        public MyCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
        }

        // The newView method is used to inflate a new view and return it,
        // you don't bind any data to the view at this point.
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(R.layout.lv_layout, parent, false);
        }

        // The bindView method is used to bind all data to a given view
        // such as setting the text on a TextView.
        @Override
        public void bindView(View view, final Context context, final Cursor cursor) {
            // Find fields to populate in inflated template
            TextView nametxt = (TextView) view.findViewById(R.id.tv_name);
            TextView detailstxt = (TextView) view.findViewById(R.id.tv_details);
            LinearLayout item = (LinearLayout) view.findViewById(R.id.item);
            // Extract properties from cursor

            String _id = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
            final String name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
            String photo = cursor.getString(cursor.getColumnIndexOrThrow("photo"));
            String details = cursor.getString(cursor.getColumnIndexOrThrow("details"));
            final String Mobile = cursor.getString(cursor.getColumnIndexOrThrow("Mobile"));
            // Populate fields with extracted properties

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent(context, ClickedContact.class);
                    mIntent.putExtra("name", name);
                    mIntent.putExtra("number", Mobile);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    view.getContext().startActivity(mIntent);

                }
            });

            nametxt.setText(name);
            detailstxt.setText(String.valueOf(details));

        }
    }
